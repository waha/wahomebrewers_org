<?php

namespace Drupal\waha_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Json;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "pages",
 *   source_module = "waha_migration",
 * )
 */
class Pages extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'jos_content' table.
    $not_cats = [2,4,5,7,9,10,11,17,18,19,22,31,37,39,41];
    $query = $this->select('jos_content', 'content')
      ->fields('content', [
          'id',
          'title',
          'introtext',
          'catid',
        ])
      ->condition ('content.catid', $not_cats, 'NOT IN');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'title' => $this->t('Title'),
      'introtext' => $this->t('Primary body text'),
      'catid' => $this->t('Joomla Category ID'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'content',
      ],
    ];
  }
}
