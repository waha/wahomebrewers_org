<?php

namespace Drupal\waha_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Json;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "store_node",
 *   source_module = "waha_migration",
 * )
 */
class StoreNode extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'jos_content' table.
    $query = $this->select('jos_content', 'c')
      ->fields('c', [
          'id',
          'title',
          'introtext',
        ])
      ->condition ('c.state',1)
      ->condition('c.catid',17);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('id' ),
      'title'   => $this->t('title' ),
      'introtext'    => $this->t('introtext'),
      'phone' => $this->t('Store phone number'),
      'email' => $this->t('Store email address'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'c',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // This example shows how source properties can be added in
    // prepareRow(). The source dates are stored as 2017-12-17
    // and times as 16:00. Drupal 8 saves date and time fields
    // in ISO8601 format 2017-01-15T16:00:00 on UTC.
    // We concatenate source date and time and add the seconds.
    // The same result could also be achieved using the 'concat'
    // and 'format_date' process plugins in the migration
    // definition.
    // $date = $row->getSourceProperty('date');
    // $time = $row->getSourceProperty('time');
    // $datetime = $date . 'T' . $time . ':00';
    // $row->setSourceProperty('datetime', $datetime);
    $tmprows = [];
    foreach (Html::load($row->getSourceProperty('introtext'))->getElementsByTagName('tr') as $tr) {
      $tmprow = [];
      foreach ($tr->getElementsByTagName('td') as $td) {
        $tmprow[] = trim($td->textContent);
      }
      $tmprows[] = $tmprow;
    }
    foreach ($tmprows as $key => $value) {
      if (preg_match('/e-?mail/i', $value[0])){
        $row->setSourceProperty('email', $value[1]);
      }
      if (preg_match('/(telephone|phone)/i', $value[0])){
        $row->setSourceProperty('phone',$value[1]);
      }
      if (preg_match('/website/i', $value[0])){
        $url = parse_url($value[1]);
        if (empty($url['scheme'])){
          $row->setSourceProperty('website', 'http://' . $value[1]);
        } else {
          $row->setSourceProperty('website', $value[1]);
        }
      }
      if (preg_match('/street address/i', $value[0])) {
        $row->setSourceProperty('street_address', $value[1]);
      }
      if (preg_match('/city/i', $value[0])) {
        $row->setSourceProperty('city', $value[1]);
      }
      if (preg_match('/zip/i', $value[0])) {
        $row->setSourceProperty('zip', $value[1]);
      }
      if (preg_match('/^(info|store information)$|other store information/i', $value[0])){
        $row->setSourceProperty('body', $value[1]);
      }
    }
    return parent::prepareRow($row);
  }
}
