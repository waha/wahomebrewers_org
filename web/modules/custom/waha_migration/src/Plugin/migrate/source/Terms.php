<?php

namespace Drupal\waha_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Json;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "terms",
 *   source_module = "waha_migration",
 * )
 */
class Terms extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'jos_content' table.
    $not_cats = [2,4,5,7,9,10,11,17,18,19,22,31,37,39,41];
    $query = $this->select('jos_categories', 'cat')
      ->fields('cat', [
          'id',
          'name',
          'description',
        ])
      ->condition ('cat.published',1)
      ->condition ('cat.id', $not_cats, 'NOT IN');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'tid'         => $this->t('id'),
      'name'        => $this->t('name'),
      'description' => $this->t('description'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'cat',
      ],
    ];
  }
}
