# Washington Homebrewers Association

This is a Drupal 8 site built with  [Composer](https://getcomposer.org/). The local development environment run using the amazee.io Docker tools. To run the site locally, please see the [amazee.io docs](https://docs.amazee.io/local_docker_development/local_docker_development.html)
